/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/8.5.35
 * Generated at: 2018-12-23 22:44:08 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = null;
  }

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
      throws java.io.IOException, javax.servlet.ServletException {

    final java.lang.String _jspx_method = request.getMethod();
    if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method) && !javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
      response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JSPs only permit GET POST or HEAD");
      return;
    }

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("\t<head>\r\n");
      out.write("\t\t<meta http-equiv=\"expires\" content=\"0\">\r\n");
      out.write("\t\t<meta charset=\"utf-8\">\r\n");
      out.write("\t\t<meta http-equiv=\"X-UA-Compatible\" comtent=\"IE=edge\">\r\n");
      out.write("\t\t<title>Pay</title>\r\n");
      out.write("\t\t<link rel=\"stylesheet\" href=\"css/bootstrap.min.css\">\r\n");
      out.write("\t\t<link href=\"https://fonts.googleapis.com/css?family=Roboto+Condensed\" rel=\"stylesheet\">\r\n");
      out.write("\t\t<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/js.js\"></script>\r\n");
      out.write("\t</head>\r\n");
      out.write("\t<style>\r\n");
      out.write("\t\tbody{\r\n");
      out.write(" \t\t\tfont-family: 'Roboto Condensed', sans-serif;\r\n");
      out.write("\t\t}\r\n");
      out.write("\t</style>\r\n");
      out.write("\t<body>\r\n");
      out.write("\t\t<div class=\"col-sm-12\">\r\n");
      out.write("\t\t\t<h1>ÐÑÐ°ÑÐ¸Ðº Ð½Ð°Ð³ÑÑÐ·ÐºÐ¸ CPU:</h1>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div class=\"col-sm-6\" style=\"margin-top: 20px;\">\r\n");
      out.write("\t\t\t<canvas id=\"myChart\"></canvas>\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<div class=\"col-sm-12\">\t\r\n");
      out.write("\t\t\t<!--<button class=\"btn\" style=\"margin-top: 20px;\" id=\"startAgain\">ÐÑÐ¾Ð¸Ð·Ð²ÐµÑÑÐ¸ Ð¿ÐµÑÐµÐ´Ð¿Ð¾Ð´ÐºÐ»ÑÑÐµÐ½Ð¸Ðµ</button>-->\r\n");
      out.write("\t\t</div>\r\n");
      out.write("\t\t<script>\r\n");
      out.write("\t\t\t/*function randomInteger(min, max) {\r\n");
      out.write("\t\t\t    var rand = min - 0.5 + Math.random() * (max - min + 1)\r\n");
      out.write("\t\t\t    rand = Math.round(rand);\r\n");
      out.write("\t\t\t    return rand;\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\tvar tempt = [];\r\n");
      out.write("\t\t\tvar time = [];\r\n");
      out.write("\t\t\tvar timeNow = 1;\r\n");
      out.write("\t\t\t/*for(var i = 0; i < 20; i++){\r\n");
      out.write("\t\t\t\ttempt.push(randomInteger(-100, 100));\r\n");
      out.write("\t\t\t\ttime.push(timeNow);\r\n");
      out.write("\t\t\t\ttimeNow++;\r\n");
      out.write("\t\t\t}\r\n");
      out.write("\t\t\tvar timeAmount = tempt.length;\r\n");
      out.write("\t\t\tvar ctx = document.getElementById('myChart');\r\n");
      out.write("\t\t\tvar chart = new Chart(ctx, {\r\n");
      out.write("\t\t\t    type: 'line',\r\n");
      out.write("\t\t\t    data: {\r\n");
      out.write("\t\t\t        labels: time,\r\n");
      out.write("\t\t\t        datasets: [{\r\n");
      out.write("\t\t\t            label: false,\r\n");
      out.write("\t\t\t            borderColor: 'rgb(255, 99, 132)',\r\n");
      out.write("\t\t\t            data: tempt\r\n");
      out.write("\t\t\t        }]\r\n");
      out.write("\t\t\t    },\r\n");
      out.write("\t\t\t    options: {\r\n");
      out.write("\t\t\t        legend: {\r\n");
      out.write("\t\t\t            display: false\r\n");
      out.write("\t\t\t        },\r\n");
      out.write("\t\t\t        scales: {\r\n");
      out.write("\t\t\t\t\txAxes: [{\r\n");
      out.write("\t\t\t\t\t\tdisplay: true,\r\n");
      out.write("\t\t\t\t\t\tscaleLabel: {\r\n");
      out.write("\t\t\t\t\t\t\tdisplay: true,\r\n");
      out.write("\t\t\t\t\t\t\tlabelString: 'ÐÑÐµÐ¼Ñ Ñ Ð½Ð°ÑÐ°Ð»Ð° Ð½Ð°Ð±Ð»ÑÐ´ÐµÐ½Ð¸Ñ, [Ñ]'\r\n");
      out.write("\t\t\t\t\t\t}\r\n");
      out.write("\t\t\t\t\t}],\r\n");
      out.write("\t\t\t\t\tyAxes: [{\r\n");
      out.write("\t\t\t\t\t\tdisplay: true,\r\n");
      out.write("\t\t\t\t\t\tscaleLabel: {\r\n");
      out.write("\t\t\t\t\t\t\tdisplay: true,\r\n");
      out.write("\t\t\t\t\t\t\tlabelString: 'ÐÐ°Ð³ÑÑÐ·ÐºÐ° CPU [%]'\r\n");
      out.write("\t\t\t\t\t\t}\r\n");
      out.write("\t\t\t\t\t}]\r\n");
      out.write("\t\t\t\t}\r\n");
      out.write("\t\t\t\t}\r\n");
      out.write("\t\t\t});*/\r\n");
      out.write("\t\t\tsetInterval(function() {\r\n");
      out.write("\t\t\t\t$.ajax({\r\n");
      out.write("\t                type: \"GET\",\r\n");
      out.write("\t                url: '/cpu',\r\n");
      out.write("\t                dataType: \"string\",\r\n");
      out.write("\t                context: this,\r\n");
      out.write("\t                success: function(response) {\r\n");
      out.write("\t                \t/*timeNow++;\r\n");
      out.write("\t\t\t\t\t\ttempt.push(parseInt(response));\r\n");
      out.write("\t\t\t\t\t\tif (tempt.length() >= 10)\r\n");
      out.write("\t\t\t\t\t\t\ttempt.shift();\r\n");
      out.write("\t\t\t\t\t\ttime.push(timeNow);\r\n");
      out.write("\t\t\t\t\t\tif (time.length() >= 10)\r\n");
      out.write("\t\t\t\t\t\t\ttime.shift();\r\n");
      out.write("\t                \tchart.update()*/\r\n");
      out.write("\t                \tconsole.log(response);\r\n");
      out.write("\t                }\r\n");
      out.write("\t            });\r\n");
      out.write("\t\t\t\t}, 1000);\r\n");
      out.write("\t\t\t/*$('#startAgain').click(function(){\r\n");
      out.write("\t\t\t\tchart.reset();\r\n");
      out.write("\r\n");
      out.write("\t\t\t});*/\r\n");
      out.write("\r\n");
      out.write("\t\t</script>\r\n");
      out.write("\r\n");
      out.write("\t</body>\r\n");
      out.write("</html>\r\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
