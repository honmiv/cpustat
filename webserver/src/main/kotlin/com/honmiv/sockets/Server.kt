package com.honmiv.sockets

import java.io.ObjectInputStream
import java.net.ServerSocket
import java.net.Socket
import java.util.regex.Pattern

object Server {

    var lastCpu = "0"
        private set
    private const val port = "10112"

    init {
        try {
            // Сокет сервер, который слушает порт port
            val listener = ServerSocket(Integer.valueOf(port))
            // Информирование о начале работе сервера
            println("Сервер занял порт: " + Integer.valueOf(port))
            // Ожидание подключения
            Thread(Runnable {
                try {
                    while (true) {
                        val clientSocket = listener.accept()
                        Thread(Runnable {
                            try {
                                exchange(clientSocket)
                            } catch (e: Exception) {
                                println(e.message)
                            }
                        }).start()
                    }
                } catch (e: Exception) {
                    println(e.message)
                }
            }).start()
        } catch (e: Exception) {
            println(e.message)
        }
    }

    private fun exchange(clientSocket: Socket) {
        try {
            // Информирование о произошедшем `подключении
            println("Выполнено подключение клиента")
            // Открытие входного потока для получения сообщений
            val inSt = ObjectInputStream(clientSocket.getInputStream())
            while (true) {
                val response = inSt.readObject()
                if (response != null) {
                    val pattern = Pattern.compile("(\\d{1,3}\\.\\d{1,2})")
                    val matcher = pattern.matcher(response.toString())
                    if (matcher.find())
                        lastCpu = matcher.group(1)
                }
            }
        } finally {
            lastCpu = "0"
        }
    }
}
