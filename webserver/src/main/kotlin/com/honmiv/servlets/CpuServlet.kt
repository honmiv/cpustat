package com.honmiv.servlets

import com.honmiv.sockets.Server
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet(name = "cpu", value = ["/cpu"])
class CpuServlet : HttpServlet() {

    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        resp.writer.write(Server.lastCpu)
    }
}