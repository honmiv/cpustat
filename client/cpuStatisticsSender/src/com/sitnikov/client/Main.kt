package com.honmiv.client

import java.io.*
import java.net.*
import java.io.InputStreamReader
import java.io.BufferedReader
import java.io.StringWriter
import java.io.Writer
import java.util.*


fun runUptimeCommand(crunchifyCmd: String, waitForResult: Boolean): String {
    var crunchifyProcessBuilder: ProcessBuilder? = null

    crunchifyProcessBuilder = ProcessBuilder("/bin/bash", "-c", crunchifyCmd)
    crunchifyProcessBuilder.redirectErrorStream(true)
    var crunchifyWriter: Writer? = null
    try {
        val process = crunchifyProcessBuilder.start()
        if (waitForResult) {
            val crunchifyStream = process.inputStream

            if (crunchifyStream != null) {
                crunchifyWriter = StringWriter()

                val crunchifyBuffer = CharArray(2048)
                try {
                    val crunchifyReader = BufferedReader(InputStreamReader(crunchifyStream, "UTF-8"))
                    var count: Int = crunchifyReader.read(crunchifyBuffer)
                    while (count != -1) {
                        crunchifyWriter.write(crunchifyBuffer, 0, count)
                        count = crunchifyReader.read(crunchifyBuffer)
                    }
                } finally {
                    crunchifyStream.close()
                }
                crunchifyWriter.toString()
                crunchifyStream.close()
            }
        }
    } catch (e: Exception) {
        println("Error Executing tcpdump command$e")
    }

    return crunchifyWriter!!.toString()
}

fun main(args: Array<String>) {
    if (args.size != 3) println("host port timeout")
    else {
        // подключение
        println("trying to connect")
        val socket = Socket(args[0], args[1].toInt())
        println("connected, openning outObjectStream")
        // открытие выходного потока для отправки собщений
        val out = ObjectOutputStream(socket.getOutputStream())
        val command = "uptime"
        var lastSendTime = Date().time
        while (true) {
            if (Date().time - lastSendTime> args[2].toLong()) {

                println("timeout expired")
                lastSendTime = Date().time
                try {
                    println("trying get uptime")
                    var cpu = runUptimeCommand(command, true)
                    println("got $cpu")
                    out.writeObject(cpu)
                    println("uptime sent")
                } catch (e: Exception) {
                    println(e.message)
                }
                println("waiting for timeout")
            }
        }
    }
}
